function Network = GenerateTopology(Parameters)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to generate the connectivity matrices
%---------------------------------------------------------------------
disp('Generating Network Topology..');

%%% Read data structures
nE = Parameters.Net.nE;
nI = Parameters.Net.nI;
nIU = Parameters.Net.nIU;
N = nE+nI;

pEE = Parameters.Topology.cEE;
pEI = Parameters.Topology.cEI;
pIE = Parameters.Topology.cIE;
pII = Parameters.Topology.cII;
lambda = Parameters.Topology.lambda;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generate topology (random connectivity matrices)
%--------------------------------------------------------------------------
if pEE
    wEE = sprand(nE, nE, pEE);
    aEE = wEE>0;
    wEE = normalizeRows(wEE);
else
    wEE = zeros(nE, nE);
    aEE = wEE;
end

if pEI
    wEI = sprand(nE, nI, pEI);
    aEI = wEI>0;
    wEI = normalizeRows(wEI);
else
    wEI = zeros(nE, nI);
    aEI = wEI;
end

if pIE
    wIE = sprand(nI, nE, pIE);
    aIE = wIE>0;
    wIE = normalizeRows(wIE);
else
    wIE = zeros(nI, nE);
    aIE = wIE;
end

if pII 
    wII = sprand(nI, nI, pII);
    aII = wII>0;
    wII = normalizeRows(wII);
else
    wII = zeros(nI, nI);
    aII = wII;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set W_in (if needed) and initialize W_out
%--------------------------------------------------------------------------
input_delivery = Parameters.Input.Inp.input_delivery;
nU = Parameters.Input.Inp.nU;
fb_Gain = Parameters.Net.fb_Gain;
in_Gain = Parameters.Net.in_Gain;


if input_delivery==1
    
    W_in = (2.0*(rand(nE,nU)-0.5));
    
elseif input_delivery<1 && input_delivery>0

    W_in = sprand(nE, nU, input_delivery);

elseif input_delivery == 2
    
        A_in = zeros(nIU*nU, nU);
        x = 1:nIU:(nIU*nU);
        y = x+(nIU-1);
        for i = 1:length(x)
            intervals{i} = [x(i), y(i)];
        end
        for i = 1:length(x)
            A_in(intervals{i}(1):intervals{i}(2), i) = 0.5;%1;
        end
        
        A_in(A_in==0) = -0.5;
        
        A_in(nU*nIU + 1:nE,:) = 0;
           

        W_in = A_in;

else
    W_in = eye(nE,nE);
end

W_in = W_in*in_Gain;

W_fb = rand(nE,nU)*fb_Gain;

W_out = zeros(nE, nU);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set initial firing thresholds and target rates
%-------------------------------------------------------------------------
tEmax = Parameters.Plasticity.tEmax;
tImax = Parameters.Plasticity.tImax;
miu_IP = Parameters.Plasticity.miu_IP;
sig_IP = Parameters.Plasticity.sig_IP;

tE = tEmax.*rand(nE,1);
tI = tImax.*rand(nI,1);

hIP = miu_IP + sig_IP.*randn(nE,1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Store variables to data structure
%==========================================================================
Network.Topology.wEE = wEE;
Network.Topology.aEE = aEE;
Network.Topology.wEI = wEI;
Network.Topology.aEI = aEI;
Network.Topology.wIE = wIE;
Network.Topology.aIE = aIE;
Network.Topology.wII = wII;
Network.Topology.aII = aII;

Network.Topology.W_in = W_in;
Network.Topology.W_out = W_out;
Network.Topology.W_fb = W_fb;

Network.Thresholds.tE = tE;
Network.Thresholds.tI = tI;

Network.Plasticity.hIP = hIP;