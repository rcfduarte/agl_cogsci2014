function Simulation = SimulateNet(Parameters, Network, Input)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Main Simulation Environment
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
plt = Parameters.Options.plt;
% Extract relevant variables
learn = Parameters.Options.Plasticity;
SimT_train = Parameters.Time.SimT_train;
SimT_test = Parameters.Time.SimT_test;
pC = Parameters.Topology.ConnRate;
nOU = Parameters.Input.Inp.nU;
nE = Parameters.Net.nE;
nI = Parameters.Net.nI;
niu_nz = Parameters.Net.niu_nz;
sig_nz = Parameters.Net.sig_nz;

u_train = Input.train_input;
u_test = Input.test_input;
u_learn = [];
target_train = Input.train_target;
target_test = Input.test_target;

wEE = full(Network.Topology.wEE);
aEE = Network.Topology.aEE;
wEI = full(Network.Topology.wEI);
aEI = Network.Topology.aEI;
wIE = full(Network.Topology.wIE);
aIE = Network.Topology.aIE;
wII = full(Network.Topology.wII);
aII = Network.Topology.aII;
W_fb = Network.Topology.W_fb;
W_in = Network.Topology.W_in;
W_out = Network.Topology.W_out;

tE = Network.Thresholds.tE;
tI = Network.Thresholds.tI;

SimT_learn = 0;
learn_rule = Parameters.Output.LearningRule;

%%% Conditional variables
if learn
    SimT_learn = Parameters.Time.SimT_learn;
    u_learn    = Input.learn_input;
    
    hIP = Network.Plasticity.hIP;
    STDP = Parameters.Plasticity.STDP;
    IP = Parameters.Plasticity.IP;
    iSTDP = Parameters.Plasticity.iSTDP;
    SP = Parameters.Plasticity.SP;
    SN = Parameters.Plasticity.SN;
    
    miu_IP = Parameters.Plasticity.miu_IP;
    niu_inh = Parameters.Plasticity.niu_inh;
    niu_IP = Parameters.Plasticity.niu_IP;
    niu_STDP = Parameters.Plasticity.niu_STDP;
    sig_IP = Parameters.Plasticity.sig_IP;
end

%##########################################################################
%                             INITIALIZE                                  %
%##########################################################################
disp('Simulating...');

%%% Total Simulation Time
if learn
    nSteps = SimT_learn+SimT_train+SimT_test;
    %%% Time series for the random generation of new connections
    newC = rand(nSteps,1)<=pC;
    
else
    nSteps = SimT_train+SimT_test;
end

%%% Weights and states
PRR.z_train       = zeros(nOU,SimT_train);   % store the output (train)
PRR.z_test        = zeros(nOU,SimT_test);    % store the output (test)

xE_0        = zeros(nE,1);               % initialize E states,
xI_0        = zeros(nI,1);               %  ... I states
z0          = 0.5*randn(nOU,1);          %  ... and outputs


%##########################################################################
%                                MAIN                                     %
%##########################################################################
%%% set initial values
xE = xE_0; xI = xI_0;
z = z0;

%%% conditional variables...
if strcmp(learn_rule, 'FORCE_RLS')
    % Initialize P matrix
    P = (1.0/Parameters.Output.FORCE_RLS.alpha)*eye(nE);
end

ct = 0;
input = [u_learn, u_train, u_test];
Parameters.Time.StartSim = tic;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%% SIMULATOR LOOP %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for t = 1:nSteps
    %%%%%%%%%%%%%%============ State noise ===========%%%%%%%%%%%%%%
    E_nz = niu_nz+sig_nz.*randn(nE,1);
    I_nz = niu_nz+sig_nz.*randn(nI,1);
    
    %%%%%%%%%%%%%%===== Update States and Outputs =====%%%%%%%%%%%%%%
    pre_xE = xE; pre_xI = xI;  % for the plasticity rules
    pre_wEE = wEE; pre_wEI = wEI;
    
    rDriveE = wEE*xE - wEI*xI + E_nz - tE;
    stE     = rDriveE + W_in*input(:,t);
    stI     = wIE*xE - wII*xI + I_nz - tI;
    
    pStateE = double(rDriveE>0);
    xE      = double(stE>0);
    xI      = double(stI>0);
    
    %%%%%%%%%%%%%%%========= PLASTICITY ==========%%%%%%%%%%%%
    if learn
        if STDP
            %-------- STDP ---------
            aEE = wEE>0;
            wEE = wEE + niu_STDP * aEE.*(xE*pre_xE'-pre_xE*xE');
            wEE(wEE<0) = 0;
        end
        if SP
            %----- Structural ------
            if newC(t) && ~any(find(wEE==0))
                [i,j] = find(wEE==0);
                idx = randperm(length(i));
                wEE(i(idx(1)),j(idx(1))) = 0.001;
            end
        end
        if iSTDP
            %------- iSTDP --------
            aEI = wEI>0;
            wEI = wEI + aEI.*(-niu_inh.*(pre_xI*(1-(xE'*...
                (1+1/miu_IP)))))';
            wEI(wEI<0) = 0;
        end
        if SN
            %--------- SN ----------
            wEE = normalizeRows(wEE);
            wEI = normalizeRows(wEI);
        end
        if IP
            %--------- IP ----------
            tE = tE+niu_IP*(pre_xE-hIP);
        end
    end
    
    
    StateVar = xE;
    
    z = W_out'*StateVar;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%============== TRAIN ============%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    if t >= SimT_learn+1 && t <= SimT_learn+SimT_train
        
        ti = t - SimT_learn;
        
        PRR.z_train(:,ti) = z;
        
        if strcmp(learn_rule, 'FORCE_RLS')
            
            % Update inverse correlation matrix
            k = P*StateVar;
            rPr = StateVar'*k;
            c = 1.0/(1.0 + rPr);
            P = P - k*(k'*c);
            
            % Update the error
            e = z - target_train(:,ti);
            
            % Update the output weights
            dW = - (k*e')*c;
            preW_out = W_out; %(store)
            W_out = W_out + dW;
           
        else
            % just store the states in a matrix for batch learning
            state_train(:,ti) = StateVar;
            
        end
        
        
        %%% Display some data after training
        if t == SimT_learn+SimT_train
            PRR.error_avg1 = mean(mean((PRR.z_train - target_train).^2));
            disp(['Training MSE: ' num2str(PRR.error_avg1,3)]);
            
            display(sprintf('Training Time: %f [sec] \n', toc(Parameters.Time.StartSim)));
        end
        
        
        if ~strcmp(learn_rule, 'batch')
            PRR.W_out_len(ti) = norm(W_out);
            PRR.z_train(:,ti) = z;
            
            if ti>=2
                W_Di     = (W_out - preW_out);
                PRR.W_der(ti)    = norm(W_Di);
            end
        else
            PRR.state_train = double(state_train);
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%============== TEST =============%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
    elseif t >= SimT_learn + SimT_train + 1
        
        ttt = t - (SimT_learn + SimT_train);
        
        PRR.z_test(:,ttt) = z;
        
        PRR.state_test(:,ttt) = StateVar;

        
        if strcmp(learn_rule, 'batch')
            state_test(:,ttt) = StateVar;
        end
        
        
        if t == nSteps
            disp('Simulation Complete');
            disp(['Total Time: ', num2str(toc(Parameters.Time.StartSim))]);
            if ~strcmp(learn_rule, 'batch')
                error_avg2 = mean(mean((PRR.z_test - target_test).^2));
                disp(['Testing MSE: ' num2str(error_avg2,3)]);
            else
                PRR.state_test = double(state_test);
            end
        end
        
    end
     
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Store variables to data structures
%==========================================================================
Simulation = PRR;
