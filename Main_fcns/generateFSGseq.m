function [seq, varargout] = generateFSGseq(Nodes, Symbols, nStrings, type, ...
    varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

seq(1)='#';
state = 1;

if strcmp(type, 'G')
    while numel(strfind(seq,'#')) < nStrings
        r = randi(nnz(Nodes(:,state)));
        seq(end+1) = Symbols(r, state);
        state = Nodes(r, state);
    end
    seq(isspace(seq)) = '';
    varargout{1} = state;
elseif strcmp(type, 'NG')
    NGratio = varargin{1};
    while numel(strfind(seq,'#')) < nStrings
        idx = randi(numel(Symbols));
        seq(end+1) = Symbols(idx);
    end
    seq(isspace(seq)) = '';
    varargout{1} = state;
elseif strcmp(type, 'GNG')
    NGratio = varargin{1};
    Gseq(1)='#';
    % Set the ratio of G/NG
    nNGstr = round(nStrings * NGratio);
    nGstr  = nStrings - nNGstr;
    %%% generate grammatical strings
    while numel(strfind(Gseq,'#')) < nGstr
        r = randi(nnz(Nodes(:,state)));
        Gseq(end+1) = Symbols(r, state);
        state = Nodes(r, state);
    end
   
    Gseq(isspace(Gseq)) = '';
    
    %%% generate non-grammatical strings
    Symbols = unique(Symbols(Symbols~=' '));
    NGseq(1) = '#';
    
    while numel(strfind(NGseq,'#')) < nNGstr
        idx = randi(numel(Symbols));
        NGseq(end+1) = Symbols(idx);
        
        l = length(NGseq);
        if l>1
            if strcmp(NGseq(l-1:l), '##')
                NGseq(l-1:l)=[];
            end
        end
        
    end
    
    %%% mix them
    StartGstr = strfind(Gseq,'#');
    StartNGstr = strfind(NGseq,'#');
    
    Gstrings  = cell(1, nGstr);
    NGstrings = cell(1, nNGstr);
    
    for i = 1:nGstr
        if i+1<=numel(StartGstr)
            Gstrings{i} = Gseq(StartGstr(i):StartGstr(i+1)-1);
        end
    end
    
    for i = 1:nNGstr
        if i+1<=numel(StartNGstr)
            NGstrings{i} = NGseq(StartNGstr(i):StartNGstr(i+1)-1);
        end
    end
    
    randSeq = horzcat(ones(1,nGstr), zeros(1,nNGstr));
    id = randperm(length(randSeq)); randSeq = randSeq(id);
    
    GstrIdx = find(randSeq==1); NGstrIdx = find(randSeq==0);
    
    seqc = cell(1, nStrings);
    for i=1:length(GstrIdx)
        seqc{GstrIdx(i)} = Gstrings{i};
    end
    for i = 1:length(NGstrIdx)
        seqc{NGstrIdx(i)} = NGstrings{i};
    end
    
    mark = cell(1,nStrings);
    
    for i = 1:nStrings
        len(i) = length(seqc{i});
        
        if randSeq(i)==1
            mark{i} = ones(1,len(i));
        else
            mark{i} = zeros(1, len(i));
        end
    end
    
    mark = cell2mat(mark);
    
    seq = [seqc{1:end}];

    seq(isspace(seq)) = '';
    varargout{1} = mark;
end
