function [Input] = GenerateInputSequences(Parameters)
%##########################################################################
%                          INPUT AND TARGET                               %
%##########################################################################
%%% Generate the symbolic sequences according to the rules of the grammar:
% (this is a mess because each task has its own different parameter
% settings) 

% Read Data Structures (extract relevant variables):
Type = Parameters.Input.Task.Type;
learn = Parameters.Options.Plasticity;
nStrings = Parameters.Input.nStrings;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if strcmp(Type, 'FSG')
    
    train_fraction = Parameters.Input.Task.train_fraction;
    test_fraction  = Parameters.Input.Task.test_fraction;
    StrTypeTe = Parameters.Input.Task.StrTypeTe;
    StrTypeTr = Parameters.Input.Task.StrTypeTr;
    NGratio   = Parameters.Input.Task.NGratio;
    grammarType = Parameters.Input.Task.grammarType;
    
    if learn
        
        learn_fraction = Parameters.Input.Task.learn_fraction;
        
        if strcmp(StrTypeTe, 'GNG')
            [S.train_seq, S.test_seq, S.learn_seq, S.mark] = genInputSeqs(Type, ...
                nStrings, learn, train_fraction, test_fraction, ...
                StrTypeTr, StrTypeTe, ...
                grammarType, learn_fraction, NGratio);
        else
            [S.train_seq, S.test_seq, S.learn_seq] = genInputSeqs(Type, nStrings, ...
                learn, train_fraction,test_fraction,...
                StrTypeTr, StrTypeTe, grammarType,...
                learn_fraction);
        end
        
    else
        if strcmp(StrTypeTe, 'GNG')
            [S.train_seq, S.test_seq, S.mark] = genInputSeqs(Type, nStrings, ...
                learn, train_fraction, test_fraction,...
                StrTypeTr, StrTypeTe, grammarType, 0,...
                NGratio);
        else
            [S.train_seq, S.test_seq] = genInputSeqs(Type, nStrings, ...
                learn, train_fraction, test_fraction,...
                StrTypeTr, StrTypeTe, grammarType);
        end
    end
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Store relevant variables in Input data structure
%==========================================================================
Input = S;
