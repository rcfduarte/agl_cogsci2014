function [train_seq, test_seq, varargout] = genInputSeqs(Type, nStrings, ...
    varargin)
%==========================================================================
%                         Generate input sequences
%--------------------------------------------------------------------------
% Generates the symbolic sequences for (learn), train and test, according
% to the specifications (Type of task, number of strings, ...)
%   - calls the function generate_symbolicSeq()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
learn = varargin{1};   % if there is an initial learning phase (SORN), a 
                       % new sequence is needed...
train_fraction = varargin{2}; % fraction of total strings used for training
test_fraction  = varargin{3};
nargout = 0;           % number of output arguments needs to be adjusted
                       % according to the tasks
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if strcmp(Type, 'FSG')
    %%%=================== Finite State Grammars =======================%%%
    StrTypeTr      = varargin{4};
    StrTypeTe      = varargin{5};    
    grammarType    = varargin{6};
    
    display(sprintf('Generating %d input strings, according to %s rules',...
        nStrings, grammarType));
    
    
    %%%#######################  LEARN  ####################################
    if learn
        learn_fraction = varargin{7};
        nargout = nargout+1;
        
        display(sprintf('    - Learn Sequence: %d %s strings (~ %2f of total)', ...
            round(nStrings*learn_fraction), StrTypeTr, learn_fraction));
 
        [learn_seq] = generate_symbolicSeq(Type, round(nStrings*learn_fraction),...
            grammarType, StrTypeTr, 0);
        learn_seq(isspace(learn_seq)) = '';
        
        varargout{nargout} = learn_seq;
    end    
    
    
    %%%#######################  TRAIN  ####################################
    display(sprintf('    - Train Sequence: %d %s strings (~ %2f of total)', ...
        round(nStrings*train_fraction), StrTypeTr, train_fraction));
    
    %$$$%% Mix Grammatical and Non-Grammatical Strings %$$$%%
    if strcmp(StrTypeTr, 'GNG')
        NGratio = varargin{8}; 
        nargout = nargout+1;
        
        [train_seq, mark] = generate_symbolicSeq(Type, ...
            round(nStrings*train_fraction), grammarType, StrTypeTr, NGratio);
        varargout{nargout} = mark;
    else
        [train_seq] = generate_symbolicSeq(Type, ...
            round(nStrings*train_fraction), grammarType, StrTypeTr);
    end
    %$$$%%---------------------------------------------%$$$%%
    
    train_seq(isspace(train_seq)) = '';
    
    
    %%%#######################  TEST  #####################################
    display(sprintf('    - Test Sequence: %d %s strings (~ %2f of total)', ...
        (round(nStrings*test_fraction)), ...
        StrTypeTe, test_fraction));
    
    %$$$%% Mix Grammatical and Non-Grammatical Strings %$$$%%
    if strcmp(StrTypeTe, 'GNG')
        NGratio = varargin{8}; 
        nargout = nargout+1;
        [test_seq, mark] = generate_symbolicSeq(Type, ...
            round(nStrings*test_fraction), grammarType, StrTypeTe, NGratio);
        varargout{nargout} = mark;
    else
        [test_seq] = generate_symbolicSeq(Type, ...
            round(nStrings*test_fraction), grammarType, StrTypeTe);
    end
    %$$$%%---------------------------------------------%$$$%%
    
    test_seq(isspace(test_seq)) = '';
    
end