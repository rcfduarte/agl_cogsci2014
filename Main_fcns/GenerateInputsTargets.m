function [Input, Parameters] = GenerateInputsTargets(Input, Parameters, Network)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generate the input and target matrices from the given sequences
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Extract variables from structure
learn = Parameters.Options.Plasticity;
dur = Parameters.Input.Inp.dur;

%%% Encode symbolic sequences according to the specified parameters
if learn
    [learn_input, learn_target] = EncodeSequence(Input.learn_seq, ...
        Parameters, Network);
end
    
[train_input, train_target] = EncodeSequence(Input.train_seq, Parameters, Network);
[test_input, test_target] = EncodeSequence(Input.test_seq, Parameters, Network);
    
    
%%% Adjust the input and target matrices (prediction task) and set the time
%%% variables
if learn
    Parameters.Time.SimT_learn = (length(Input.learn_seq)*dur)-dur;
    Input.learn_input  = learn_input(:,1:Parameters.Time.SimT_learn);
    Input.learn_target = learn_target(:, dur+1:end);
end

Parameters.Time.SimT_train = (length(Input.train_seq)*dur)-dur;
Parameters.Time.SimT_test  = (length(Input.test_seq)*dur)-dur;

train_input = train_input(:,1:Parameters.Time.SimT_train);
test_input  = test_input(:,1:Parameters.Time.SimT_test);
train_target = train_target(:, dur+1:end);
test_target  = test_target(:, dur+1:end);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Store Variables in the Data structure
%==========================================================================
Input.train_input = train_input;
Input.test_input = test_input;

Input.train_target = train_target;
Input.test_target = test_target;

