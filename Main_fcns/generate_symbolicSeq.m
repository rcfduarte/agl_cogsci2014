function [seq, varargout] = generate_symbolicSeq(Type, nStrings, varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generates a sequence of strings according to the desired rules
%--------------------------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if strcmp(Type, 'FSG')
	type = varargin{1};
    
    strings = varargin{2};
        
    if isequal(type, 'RG1')
        
        Nodes = [2 2 2 4 3 7 1; 3 5 4 6 6 0 0; 0 0 0 7 7 0 0];
        
        Symbols = ['M', 'T', 'X', 'R', 'R', blanks(1), '#'; ...
            'V', 'V', 'X', 'M', 'T', blanks(2); blanks(7)];
           
    elseif isequal(type, 'KnSq')
        
        Nodes = [1 3 1 4 4 1; 2 4 6 6 6 0; 0 5 0 0 0 0];
        
        Symbols = ['X', 'J', 'T', 'J', 'V', '#'; 'V', 'X', blanks(4);...
            blanks(1), 'T', blanks(4)];
    end
    
    if isequal(strings, 'NG') || isequal(strings, 'GNG')
        NGratio = varargin{3};
        [seq, mark] = generateFSGseq(Nodes, Symbols, nStrings, strings,...
            NGratio);
        varargout{1} = mark;
    else
        [seq] = generateFSGseq(Nodes, Symbols, nStrings, strings);
    
        varargout{1} = [];
    end
    
end
