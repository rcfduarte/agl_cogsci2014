function [inputMat, targetMat] = EncodeSequence(seq, Parameters, Network)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generate the input and target matrices from the given sequences
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
input_delivery = Parameters.Input.Inp.input_delivery;

% Analyse the sequence given and create a structure of nSym binary
% variables, each of which contains a timeseries of length = len(seq)
% corresponding to the time points when each symbol is present...
unSym = unique(seq); nSym = length(unSym);
for i = 1:nSym
    u_hats{i} = genvarname(sprintf('u_hat%s', num2str(i)));
    us{i}     = genvarname(sprintf('u%s', num2str(i)));
    u_h.(u_hats{i}) = zeros(1, length(seq));
end

for t = 1:length(seq)
    for i = 1:length(unSym)
        if strcmp(seq(t), unSym(i))
            u_h.(u_hats{i})(t) = 1;
        end
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dur = Parameters.Input.Inp.dur;
amp = Parameters.Input.Inp.amp;

%%%% square pulses of duration dur and amplitude amp
inp_len = 1:dur:(length(seq)*dur)+1;

if input_delivery
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% inputs weighted in - meaning nIU = nOU = numel(Sym)
    
    nIU = numel(unSym);
    Inp = zeros(nIU, length(seq)*dur);
    
    for t = 1:length(seq)
        for i = 1:nIU
            if u_h.(u_hats{i})(t) == 1
                Inp(i, inp_len(t):(inp_len(t+1)-1)) = amp;
            end
        end
    end
    
    inputMat = Inp;
    targetMat = inputMat;
    
else
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% symbol-specific neurons
    
    nIU = Parameters.Net.nIU;
    nOU = nSym;
    nE = Parameters.Net.nE;
    
    %%%
    % just choose the first nIU neurons...
    Symbols = zeros(nIU*nSym, nSym);
    x = 1:nIU:(nIU*nSym);
    y = x+(nIU-1);
    for i = 1:length(x)
        intervals{i} = [x(i), y(i)];
    end
    for i = 1:length(x)
        Symbols(intervals{i}(1):intervals{i}(2), i) = amp;
    end
    Symbols(nSym*nIU + 1:nE,:) = 0;
    
    Inp = zeros(nE, length(seq)*dur);
    % loop through the sequence...
    for t = 1:length(seq)
        for i = 1:nSym
            if u_h.(u_hats{i})(t) == 1
                Inp(:, inp_len(t):(inp_len(t+1)-1)) = repmat(...
                    Symbols(:,i),1,dur);
            end
        end
    end
    
    inputMat = Inp;
    
    % target matrix will be different from the input (binary
    % vectors of dimensionality nOU = nSym)
    Tget = zeros(nOU, length(seq)*dur);
    
    for t = 1:length(seq)
        for i = 1:nOU
            if u_h.(u_hats{i})(t) == 1
                Tget(i, inp_len(t):(inp_len(t+1)-1)) = 1;
            end
        end
    end
    targetMat = Tget;
end

end