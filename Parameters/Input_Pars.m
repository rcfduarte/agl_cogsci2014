learn = Parameters.Options.Plasticity;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Input %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
TaskP.Type = 'FSG';                      % Type of task: 
                                         %   - FSG 

%%% Number of strings %%%                                         
nStrings = 100000; 						 % Total strings (train+test)
nLStrings = 10000;						 % learn phase (for plastic networks)

%%% Encoding properties %%% 
Inp.input_delivery = 1;              % Type of input encoding:
                                     % - weighted -> with value in ]0,1]
                                     % specifying the W_in density
                                     % - input specific populations (0)
                                     % - weighted input delivered to
                                     % specific populations including 
                                     % lateral inhibition (2)
                                                                        
% --- Extra conditional parameters --- %                                   
Inp.amp = 1;                   % pulse amplitude (input drive)
Inp.dur = 1;                   % pulse duration (time-steps)

%%%%%%%%%%%%%%%%%%%% --- Task-Specific Parameters --- %%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%  FSG  %%%%%%%%%%%%%%%%%%%%%%%
TaskP.grammarType = 'RG1';   % choices are: RG1/KnSq
TaskP.StrTypeTr = 'G';       % type of strings used in training (G/NG/GNG)
TaskP.StrTypeTe = 'G';       % type of strings used in test (G/NG/GNG)
TaskP.NGratio = 0.5;         % percentage of non-grammatical strings in the
                             % sequence (if applicable - GNG)              
   
if strcmp(TaskP.grammarType,'RG1') 
    TaskP.nGram = 3;
    Inp.nU = 6;
elseif strcmp(TaskP.grammarType, 'KnSq')
    TaskP.nGram = 5;
    Inp.nU = 5;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Store Variables in the DataStructures
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Parameters.Input.Task = TaskP;
Parameters.Input.nStrings = nStrings;
Parameters.Input.Inp = Inp;

clearvars -except Parameters Network