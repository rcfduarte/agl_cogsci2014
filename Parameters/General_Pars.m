%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Setting Parameters...');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%% Simulation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
learn = 1;                           % plasticity mechanisms on/off = 1/0;

%%% Number of neurons %%%
nE       = 500; 
nI       = .2*nE;                    
nIU      = 0.05*nE;

%%% Connection Probabilities %%%
pEE = 0.1;
pEI = 0.2;
pIE = 1;
pII = 0;
lambda = pEE*nE;                     % average number of connections per neuron

%%% Gains %%%
in_Gain = 1;						 % input gain
fb_Gain = 0;						 % feedback gain

%%% Initial Thresholds %%%
tEmax = 1;                          % maximum initial threshold value
tImax = 0.5;                            

%%% Noise parameters %%%
niu_nz = 0;             
sig_nz = sqrt(0.04);

%%% Plasticity parameters and rates %%%
% presence(1)/absence(0) of each mechanism and respective parameters
STDP = 1;
niu_STDP = 0.004;

IP = 1;
niu_IP   = 0.001;
miu_IP   = 0.1;
sig_IP   = 0;

iSTDP = 1;
niu_inh  = 0.001;

SP = 1;
pC  = 0.001;                        % create new connections (rate)

SN = 1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Learn %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
learn_rule = 'FORCE_RLS';          % learning rule:
                                   % -> 'FORCE_RLS'
                                   % -> 'batch'

% --- Extra Parameters for FORCE_RLS --- %
if strcmp(learn_rule, 'FORCE_RLS')
    Parss.alpha = 1;
elseif strcmp(learn_rule, 'batch')
    Parss = [];
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Print relevant information
%==========================================================================
disp(['   Total Size: ', num2str(N), ' (', num2str(nE), 'E + ', ...
    num2str(nI), 'I)']);
disp(['   Plasticity: ', num2str(learn)]);
disp(['   Learning Rule: ', learn_rule]);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Save in a Data structure
%==========================================================================
Parameters.Net.nE = nE;
Parameters.Net.nI = nI;
Parameters.Net.nIU = nIU;
Parameters.Net.niu_nz = niu_nz;             
Parameters.Net.sig_nz = sig_nz;
Parameters.Net.in_Gain = in_Gain;
Parameters.Net.fb_Gain = fb_Gain;

Parameters.Topology.cEE = pEE;
Parameters.Topology.cEI = pEI;
Parameters.Topology.cIE = pIE;
Parameters.Topology.cII = pII;
Parameters.Topology.lambda = lambda;
Parameters.Topology.ConnRate = pC;

Parameters.Plasticity.tEmax = tEmax;
Parameters.Plasticity.tImax = tImax;
Parameters.Plasticity.niu_STDP = niu_STDP;                    
Parameters.Plasticity.niu_IP   = niu_IP;
Parameters.Plasticity.miu_IP   = miu_IP;
Parameters.Plasticity.sig_IP   = sig_IP;
Parameters.Plasticity.niu_inh  = niu_inh;
Parameters.Plasticity.STDP = STDP;
Parameters.Plasticity.IP = IP;
Parameters.Plasticity.SP = SP;
Parameters.Plasticity.SN = SN;
Parameters.Plasticity.iSTDP = iSTDP;

Parameters.Options.Plasticity = learn;

Parameters.Output.LearningRule = learn_rule;
Parameters.Output.(genvarname(learn_rule)) = Parss;


clearvars -except Parameters Network