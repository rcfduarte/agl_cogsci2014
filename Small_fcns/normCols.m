function y = normCols(M)
% Normalize the columns of matrix M

for i=1:size(M,2)
    if nnz(M(:,i)>0)    % for very sparse matrices...
        y(:,i) = M(:,i)./sum(M(:,i));
    end
end
