function [nW]=normalizeRows(W)
% Normalize the rows of a matrix W
    
    for i = 1:size(W,1)
        if nnz(W(i,:)>0)    % for very sparse matrices...
            nW(i,:) =  W(i,:)./sum(W(i,:));
        end
    end
%    c=size(W,2); nW=diag(1./(W*ones(c,1)))*W;
end