clear all; clc; disp('Clearing workspace...');

addpath('./Parameters', './Performance', './Small_fcns', './Main_fcns',...
    './Analysis', './Plot_fcns');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                               MAIN                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

display('################################################################');
display('#               RNN Simulation (Spiking model)                 #');
display('################################################################');

%--------------------------------------------------------------------------
Parameters.Time.start = tic;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Set Parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
General_Pars
Input_Pars
%%%

Top = toc(Parameters.Time.start); 
display(sprintf('Elapsed Time: %f [sec]\n', Top));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Create Network Topology
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Network = GenerateTopology(Parameters);
%%%

Top = toc(Parameters.Time.start); 
display(sprintf('Elapsed Time: %f [sec]\n', Top));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Create input data structures
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[Input] = GenerateInputSequences(Parameters);

[Input, Parameters] = GenerateInputsTargets(Input, Parameters, Network);
%%%

Top = toc(Parameters.Time.start); 
display(sprintf('Elapsed Time: %f [sec] \n', Top));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Run Simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Simulation = SimulateNet(Parameters, Network, Input);
%%%

Top = toc(Parameters.Time.start);
display(sprintf('Elapsed Time: %f [sec] \n', Top));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Compute Performance
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[Results] = PredictionPerformance(Parameters, Input, Simulation);


