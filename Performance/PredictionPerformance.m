function [Results] = PredictionPerformance(Parameters, Input, Simulation)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute prediction performance 
%==========================================================================
disp('Estimating Performance...')

% Extract relevant variables from data structure:
learn_rule = Parameters.Output.LearningRule;
target_train = Input.train_target;
dur  = Parameters.Input.Inp.dur;
SimT_test = Parameters.Time.SimT_test;
target_test = Input.test_target;
Type = Parameters.Input.Task.Type;

%%% Conditional on the nature of the learning rule (compute W_out if
%%% offline)
if strcmp(learn_rule, 'batch')
    %%% Train Readout / Estimate output 
    state_train = Simulation.state_train;
    state_test = Simulation.state_test;
       
    [W_out] = estimateReadoutW(state_train, target_train);
    
    z_test = W_out * state_test;

else
    
    z_test = Simulation.z_test;
    
end

%%% colapse the output and target to a discrete variable...
if dur > 1
    idx = 0:dur:SimT_test;
    for i = 1:length(idx)-1
        Results.estimation(:,i) = mean(z_test(:,idx(i)+1:idx(i+1)),2);
        Results.correct(:,i)    = mean(target_test(:, idx(i)+1:idx(i+1)),2);
    end
else
    Results.estimation = z_test;
    Results.correct    = target_test;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Call the various functions to compute performance 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if strcmp(Type, 'FSG')
    grammarType = Parameters.Input.Task.grammarType;
    nGram       = Parameters.Input.Task.nGram;

    [Results.RMSE, Results.mPerf, Results.MSE] = ComputePerformance(...
        Results.estimation, Results.correct, Type, 'Exact', ...
        eps, Input.test_seq);

    %%% The order of the process (nGram) has to be set
    %%% Load the target probailities
    load(['../Input/nGramModels/',sprintf('%dGram%s.mat',...
        nGram, grammarType)]);
    
    [target_pr] = ComputeTargetDist(nGram, grammarType, Input.test_seq,...
        Seq, Prob);

    estim = Results.estimation(:, (nGram-1):end);
    
    [Results.RMSE_th, Results.mPerf_th, Results.KL, Results.Div, ...
        Results.MSE_th, Results.NNL, Results.COS] = ComputePerformance(...
        estim, target_pr, Type, 'Theoretical', eps, Input.test_seq);
end