function NL = NormNegLogLik(x, pVect1, pVect2)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Computes the Negative log likelihood ...
%==========================================================================

NL = sum(pVect1 .* (log2(pVect2)/log2(length(x))));

