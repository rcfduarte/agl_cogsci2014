function [Perf, varargout] = computeKLPerf(estimation, correct)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute Performance in terms of normalized KL divergence between netowrk
% output and target probability distributions
%--------------------------------------------------------------------------

x = [1:size(estimation,1)]';

for i = 1:size(estimation,2)
    
    KL(i) = kldiv(x, normCols(correct(:,i)+eps), ...
        normCols(estimation(:,i)+eps));
    Div(i) = 1-exp(-KL(i));
end


Perf = 1-(mean(Div));
varargout{1} = KL;
varargout{2} = Div;