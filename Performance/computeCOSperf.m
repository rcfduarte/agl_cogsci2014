function COS = computeCOSperf(correct, estimation, seq)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Computes the cosine score between the 2 normalized vectors (prob.
% distributions) - metric sometimes used in the cognitive sciences to
% assess prediction performance
%==========================================================================

T = length(correct);
x = [1:numel(unique(seq))]';

for i = 1:T
    Cs(i) = CosineProb(x, correct(:,i), estimation(:,i));
end

COS = mean(Cs);