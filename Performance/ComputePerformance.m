function [RMSE, mPerf, varargout] = ComputePerformance(estimation,...
    correct, Type, Method, varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute prediction performance and error
%--------------------------------------------------------------------------
% Takes as arguments:
%   - estimation -> network output ( nSymbols x T )
%   - correct -> target output ( nSymbols x T )
%   - Type -> nature of the task (FSG)
%   - Method:
%       o Exact = determined the network's ability to exactly predict the 
%           next symbol in the sequence
%       o Theoretical = estimation is taken as a next-symbol probability
%           distribution and compared to a target distribution (this is
%           dependent on the task specifications...)
%       o Train = computes target distribution based on the statistics 
%           of the training data
%==========================================================================
%--------------------------------------------------------------------------
o_min = varargin{1};
seq   = varargin{2};
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if strcmp(Type, 'FSG') 
    %%%-----------------------------------------------------------------%%%
    if strcmp(Method, 'Exact')
        % Winner-takes-all:
        [~, idx_est] = max(estimation, [], 1);
        [~, idx_cor] = max(correct, [], 1);
        
        % mean performance:
        prdiff = (idx_est==idx_cor); mPerf = mean(prdiff);
        
        % root mean squared error:
        sqErr = (correct-estimation).^2;
        MSE   = mean(sqErr);
        RMSE  = sqrt(MSE);
        
        %
        varargout{1} = MSE;
    %%%-----------------------------------------------------------------%%%    
    elseif strcmp(Method, 'Theoretical')
        % Interpret the output as a prob distribution for the next symbol:
        estimation(estimation <= o_min) = o_min;
        estimation = normCols(estimation);

        % Mean Performance computed as the average normalized KL divergence
        % between estimated probabilities and target
        [mPerf, KL, Div] = computeKLPerf(estimation, correct);
        
        % Performance evaluated by the normalized negative log likelihood
        NNL = computeNNLperf(seq, correct, estimation);
        
        % Performance evaluated by the cosine score:
        COS = computeCOSperf(correct, estimation, seq);
        
        % root mean squared error:
        sqErr = (correct-estimation).^2;
        MSE   = mean(sqErr);
        RMSE  = sqrt(MSE);

        %
        varargout{1} = KL;
        varargout{2} = Div;
        varargout{3} = MSE;
        varargout{4} = NNL;
        varargout{5} = COS;
    end
end