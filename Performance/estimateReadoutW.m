function [wOut] = estimateReadoutW(state, teach)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%
wOut = (pinv(state)' * teach')';
