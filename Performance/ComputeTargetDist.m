function Pr = ComputeTargetDist(n, grammarType, seq, Seq, Prob)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute the target probability distribution under a given n-gram model
%==========================================================================

% set input units (needed as argument to generate sequence)
if strcmp(grammarType,'RG1') || strcmp(grammarType, 'RG2')
    nInputUnits = 6;
elseif strcmp(grammarType,'ERG')
    nInputUnits = 8;
elseif strcmp(grammarType, 'KnSq')
    nInputUnits = 5;
end

%--------------------------------------------------------------------------
unSym = unique(seq);

nSeq = length(seq);

if n==1
    Pr = repmat(Prob', [1, length(seq)]);
else
    for ii = 1:nSeq%(n-1):nSeq
        
        for iii = 1:length(unSym)
            if ii+(n-2) < nSeq
                tg = [seq(ii:ii+(n-2)),unSym(iii)];
                id = find(strcmp(tg, Seq));
                
                if isempty(id)
                    Pr(iii,ii) = 0;
                else
                    Pr(iii,ii) = Prob{id};
                end
            end
        end
    end
end

   